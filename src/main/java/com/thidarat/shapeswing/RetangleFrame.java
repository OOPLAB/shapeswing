/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.shapeswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class RetangleFrame extends JFrame {

    JLabel lblWidth;
    JTextField txtWidth;
    JLabel lblHeight;
    JTextField txtHeight;
    JButton btnCalculate;
    JLabel lblResult;

    public RetangleFrame() {
        super("Rectangle");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(null);// การจัดตำเเหน่งบนframe

        lblWidth = new JLabel("width : ", JLabel.TRAILING); //ข้อความ
        lblWidth.setLocation(5, 5);
        lblWidth.setSize(50, 20);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        this.add(lblWidth);

        txtWidth = new JTextField(); // ช่องว่าง ใส่ 
        txtWidth.setLocation(60, 5);
        txtWidth.setSize(50, 20);
        txtWidth.setBackground(Color.WHITE);
        txtWidth.setOpaque(true);
        this.add(txtWidth);

        lblHeight = new JLabel("height : ", JLabel.TRAILING); //ข้อความ
        lblHeight.setLocation(120, 5);
        lblHeight.setSize(50, 20);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);

        txtHeight = new JTextField(); // ช่องว่าง ใส่
        txtHeight.setLocation(180, 5);
        txtHeight.setSize(50, 20);
        txtHeight.setBackground(Color.WHITE);
        txtHeight.setOpaque(true);
        this.add(txtHeight);

        btnCalculate = new JButton("Calculate"); //ปุ่ม
        btnCalculate.setLocation(250, 5);
        btnCalculate.setSize(100, 20);
        btnCalculate.setBackground(Color.WHITE);
        btnCalculate.setOpaque(true);
        this.add(btnCalculate);

        lblResult = new JLabel("Rectangle width = ??  height = ??  area = ??  perimeter = ??"); //ข้อความ 
        lblResult.setLocation(0, 100);
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setBackground(Color.WHITE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() { //  ActionListener class การสร้าง class เมื่อกดปุ่ม จะไปไปเรียก การทำงานใน actionPerformed ทำงาน

            @Override
            public void actionPerformed(ActionEvent e) {

                try { // try - catch
                    //1.ดึงข้อมูลจาก  text  จาก  txtWidth -> strWidht 
                    //txtHeight -> strHeight
                    String strWidht = txtWidth.getText();
                    String strHeight = txtHeight.getText();
                    //2. เเปลง  strWidht -> width: double parseDouble
                    //เเปลง  strHeight -> height : double parseDouble
                    double widht = Double.parseDouble(strWidht);
                    double height = Double.parseDouble(strHeight);
                    //3. instance object Retangle (widht,height) -> rectangle
                    Retangle rectangle = new Retangle(widht, height);
                    //4. update lblResult โดยนำข้อมูลจาก rectangle ไปเเสดงให้ครบถ้วน
                    lblResult.setText("Rectangle width = " + rectangle.getWidth() + "  height = "
                            + rectangle.getHeight() + " area : " + String.format("%.2f", rectangle.calArera())
                            + " perimeter: " + String.format("%.2f", rectangle.calPerimeter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RetangleFrame.this, "Error : Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText(" ");
                    txtHeight.setText(" ");
                    txtWidth.requestFocus();
                    txtHeight.requestFocus();
                }
            }
        });

    }

    public static void main(String[] args) {
        RetangleFrame frame = new RetangleFrame();
        frame.setVisible(true);
    }
}
