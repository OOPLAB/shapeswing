/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.shapeswing;

/**
 *
 * @author User
 */
public abstract class Shape { // abstract class
    private String shapeName;

    public Shape(String shapeName) { // constructor
        this.shapeName = shapeName;
    }

    public String getShapeName() {
        return shapeName;
    }

    public void setShapeName(String shapeName) {
        this.shapeName = shapeName;
    }
    
    public abstract double calArera(); // abstract method
    public abstract double calPerimeter(); // abstract method
    
    
}
