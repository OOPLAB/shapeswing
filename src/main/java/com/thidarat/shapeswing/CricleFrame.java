/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.shapeswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class CricleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Crical"); // หน้าจอ
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLayout(null);// การจัดตำเเหน่งบนframe

        JLabel lblRadius = new JLabel("radius : ", JLabel.TRAILING); //ข้อความ
        lblRadius.setLocation(5, 5);
        lblRadius.setSize(50, 20);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        frame.add(lblRadius);

        final JTextField txtRadius = new JTextField(); // ช่องว่าง ใส่ // final เพื่อการเรียกใช้ใน  ActionListener class
        txtRadius.setLocation(60, 5);
        txtRadius.setSize(50, 20);
        txtRadius.setBackground(Color.WHITE);
        txtRadius.setOpaque(true);
        frame.add(txtRadius);

        JButton btnCalculate = new JButton("Calculate"); //ปุ่ม
        btnCalculate.setLocation(120, 5);
        btnCalculate.setSize(100, 20);
        btnCalculate.setBackground(Color.WHITE);
        btnCalculate.setOpaque(true);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Cricle radius = ??  area = ??  perimeter = ??"); //ข้อความ ใส่ // final เพื่อการเรียกใช้ใน  ActionListener class
        lblResult.setLocation(0, 100);
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setBackground(Color.WHITE);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        //Event Driven ตอบสนองตามเหตุการณ์
        btnCalculate.addActionListener(new ActionListener() { //  ActionListener class การสร้าง class เมื่อกดปุ่ม จะไปไปเรียก การทำงานใน actionPerformed ทำงาน

            @Override
            public void actionPerformed(ActionEvent e) {

                try { // try - catch
                    //1.ดึงข้อมูลจาก  text  จาก   txtRadius -> strRadius
                    String strRadius = txtRadius.getText();
                    //2. เเปลง  strRadius -> radius : double parseDouble
                    double radius = Double.parseDouble(strRadius);
                    //3. instance object Cricle (radius) -> cricle
                    Cricle cricle = new Cricle(radius);
                    //4. update lblResult โดยนำข้อมูลจาก circle ไปเเสดงให้ครบถ้วน
                    lblResult.setText("Cricle r = " + cricle.getRadius() + " area : " + String.format("%.2f", cricle.calArera())
                            + " perimeter: " + String.format("%.2f", cricle.calPerimeter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error : Please input number",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText(" ");
                    txtRadius.requestFocus();
                }
            }
        });

        frame.setVisible(true);

    }
}
