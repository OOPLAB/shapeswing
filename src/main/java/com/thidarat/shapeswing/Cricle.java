/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.shapeswing;

/**
 *
 * @author User
 */
public class Cricle extends Shape { // class Cricle extends Shape

    private double radius;

    public Cricle(double radius) {
        super("Cricle");
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double calArera() { // หาพื้นที่วงกลม
        return Math.PI*Math.pow(radius, 2);
    }

    @Override
    public double calPerimeter() { //หาเส้นรอบรูปวงกลม
        return 2*Math.PI*radius;
    }

}
