 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.shapeswing;

/**
 *
 * @author User
 */
public class Retangle extends Shape {
    private double width;
    private double height;
    public Retangle(double width,double height) {
        super("Retangle");
        this.width=width;
        this.height=height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }
    

    @Override
    public double calArera() {
       return width*height;
    }

    @Override
    public double calPerimeter() {
         return (2*width)+(2*height);
    }
    
}
