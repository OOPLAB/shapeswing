/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.shapeswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class SquareFrame extends JFrame {

    JLabel lblSide;
    JTextField txtSide;
    JButton btnCalculate;
    JLabel lblResult;

    public SquareFrame() {
        super("Square");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(null);// การจัดตำเเหน่งบนframe

        lblSide = new JLabel("side : ", JLabel.TRAILING); //ข้อความ
        lblSide.setLocation(5, 5);
        lblSide.setSize(50, 20);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        this.add(lblSide);

        txtSide = new JTextField(); // ช่องว่าง ใส่ 
        txtSide.setLocation(60, 5);
        txtSide.setSize(50, 20);
        txtSide.setBackground(Color.WHITE);
        txtSide.setOpaque(true);
        this.add(txtSide);
        
        btnCalculate = new JButton("Calculate"); //ปุ่ม
        btnCalculate.setLocation(120, 5);
        btnCalculate.setSize(100, 20);
        btnCalculate.setBackground(Color.WHITE);
        btnCalculate.setOpaque(true);
        this.add(btnCalculate);

        lblResult = new JLabel("Square side = ??  area = ??  perimeter = ??"); //ข้อความ 
        lblResult.setLocation(0, 100);
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setBackground(Color.WHITE);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
         btnCalculate.addActionListener(new ActionListener() { //  ActionListener class การสร้าง class เมื่อกดปุ่ม จะไปไปเรียก การทำงานใน actionPerformed ทำงาน

            @Override
            public void actionPerformed(ActionEvent e) {

                try { // try - catch
                    //1.ดึงข้อมูลจาก  text  จาก  txtSide -> strSide 
                    String strSide = txtSide.getText();
                    //2. เเปลง  strSide -> side: double parseDouble
                    double side = Double.parseDouble(strSide);
                    //3. instance object Square (side) -> square
                    Square square = new Square(side);
                    //4. update lblResult โดยนำข้อมูลจาก square ไปเเสดงให้ครบถ้วน
                    lblResult.setText("Square side = " + square.getSide()  + " area : " + String.format("%.2f", square.calArera())
                            + " perimeter: " + String.format("%.2f", square.calPerimeter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error : Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText(" ");
                    txtSide.requestFocus();
                   
                }
            }
        });

    }

    public static void main(String[] args) {
        SquareFrame frame = new SquareFrame();
        frame.setVisible(true);
    }
}
