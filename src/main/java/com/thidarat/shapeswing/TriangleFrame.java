/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.shapeswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
public class TriangleFrame extends JFrame {

    JLabel lblBase;
    JTextField txtBase;
    JLabel lblHeight;
    JTextField txtHeight;
    JButton btnCalculate;
    JLabel lblResult;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(null);// การจัดตำเเหน่งบนframe

        lblBase = new JLabel("base : ", JLabel.TRAILING); //ข้อความ
        lblBase.setLocation(5, 5);
        lblBase.setSize(50, 20);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        this.add(lblBase);

        txtBase = new JTextField(); // ช่องว่าง ใส่ 
        txtBase.setLocation(60, 5);
        txtBase.setSize(50, 20);
        txtBase.setBackground(Color.WHITE);
        txtBase.setOpaque(true);
        this.add(txtBase);
        
        lblHeight = new JLabel("height : ", JLabel.TRAILING); //ข้อความ
        lblHeight.setLocation(120, 5);
        lblHeight.setSize(50, 20);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);

        txtHeight = new JTextField(); // ช่องว่าง ใส่
        txtHeight.setLocation(180, 5);
        txtHeight.setSize(50, 20);
        txtHeight.setBackground(Color.WHITE);
        txtHeight.setOpaque(true);
        this.add(txtHeight);

        btnCalculate = new JButton("Calculate"); //ปุ่ม
        btnCalculate.setLocation(250, 5);
        btnCalculate.setSize(100, 20);
        btnCalculate.setBackground(Color.WHITE);
        btnCalculate.setOpaque(true);
        this.add(btnCalculate);

        lblResult = new JLabel("Triangle base = ??  height = ??  area = ??  perimeter = ??"); //ข้อความ 
        lblResult.setLocation(0, 100);
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setBackground(Color.WHITE);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        
        btnCalculate.addActionListener(new ActionListener() { //  ActionListener class การสร้าง class เมื่อกดปุ่ม จะไปไปเรียก การทำงานใน actionPerformed ทำงาน

            @Override
            public void actionPerformed(ActionEvent e) {

                try { // try - catch
                    //1.ดึงข้อมูลจาก  text  จาก  txtBase -> strBase 
                    //txtHeight -> strHeight
                    String strBase = txtBase.getText();
                    String strHeight = txtHeight.getText();
                    //2. เเปลง  strBase -> base: double parseDouble
                    //เเปลง  strHeight -> height : double parseDouble
                    double base = Double.parseDouble(strBase);
                    double height = Double.parseDouble(strHeight);
                    //3. instance object Triangle (base,height) -> rectangle
                    Triangle Triangle = new Triangle(base, height);
                    //4. update lblResult โดยนำข้อมูลจาก triangle ไปเเสดงให้ครบถ้วน
                    lblResult.setText("Triangle  base = " + Triangle.getBase() + "  height = "
                            + Triangle.getHeight() + " area : " + String.format("%.2f", Triangle.calArera())
                            + " perimeter: " + String.format("%.2f", Triangle.calPerimeter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error : Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText(" ");
                    txtHeight.setText(" ");
                    txtBase.requestFocus();
                    txtHeight.requestFocus();
                }
            }
        });

    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}
