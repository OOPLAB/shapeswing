/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.shapeswing;

/**
 *
 * @author User
 */
public class Triangle extends Shape {
    private double base;
    private double height;

    public Triangle(double base, double height) {
        super("Triangle");
        this.base = base;
        this.height = height;
    }

    public double getBase() {
        return base;
    }

    public double getHeight() {
        return height;
    }
    

    @Override
    public double calArera() {
        return 0.5*base*height;
    }

    @Override
    public double calPerimeter() {
       double c=Math.sqrt((Math.pow(base,2))+Math.pow(height, 2));
        return c+base+height ; 
    }
    
}
